# HuskySat-1 camera data 

## Overview

HuskySat-1 is a satellite built by University of Washington with collaboration from a number of other schools and industry partners. The primary mission objective is testing the on-board plasma thruster and the 24 GHz patch antenna transmitter. 

Volunteers from Quick2space.org and students from Raisbeck Aviation High School designed, built, tested and delivered the CAMERA board onboard the satellite. 

For more information about HuskySat-1 please see the main page here: https://sites.google.com/uw.edu/huskysatellitelab/huskysat-1 

## Data capture

In the first stage of the mission, HuskySat- normally transmits telemetry data. When the "science mode" is enabled from the ground it also transmits more data such as Camera data. 

The data is captured by a network of FoxTelem-enabled ground stations and centralized in a common location. 

Any HAM radio amateur operating a 144 or 433 MHz antenna is encouraged to use FoxTelem to help retrieving data from AMSAT Fox's series of cubesats and make it public

https://www.amsat.org/foxtelem-software-for-windows-mac-linux/ 

## How is the image encoded in CAN packets? 

HuskySat-1 camera images has a (96x64) resolution, with 4-bit grayscale values per pixel. Each received radio packet has the following components: a 5-byte preamble, a 4-byte CAN ID, and a 8-byte CAN data. 

We use the SSF format (Simple Streaming Format) version 0.3, developed by Quick2space with assistance from University of Washington in defining the requirements. This image format was specially designed to enable image transmission over very noisy channels and yet offer some ability to distinguish noise from valid data to enable subsequent interpolation. 

For the CAMERA packets, in a nutshell, each 8-byte CAN packet contains the following information:

- 1 byte: X offset for the first pixel in the group of ten pixels 
- 1 byte: Y offset for the first pixel. Subsequent pixels will follow the first one on the same row or the next one. 
- 5 bytes: 10 pixel values (4 bit per pixel)
- 1 byte: checksum 

A detailed explanation of the image format is documented here: https://bitbucket.org/Quick2space/mcp25625.py/src/master/docs/SSF-v03.pdf 

## How to extract HuskySat-1 camera data 

- Install Python 3.x locally (http://python.org) 
- Download the AMSAT camera extraction script for HuskySat-1 from: https://bitbucket.org/Quick2space/mcp25625.py/src/master/scripts/read_amsat_camera_data.py 
- Create a new directory and run the script into this directory

```
python3 ../scripts/read_amsat_camera_data.py 
```

This procedure will generate a number of PNG images. Each PNG image name is prefixed with the date/time of the first packet that was received from the image. 

## Example output

An extracted output is located here: https://bitbucket.org/Quick2space/mcp25625.py/src/master/camera_data/2020.02.11/ 

This contains the first eight pictures received by HuskySat-1 

```
02/11/2020  12:07 PM    <DIR>          .
02/11/2020  12:07 PM    <DIR>          ..
02/11/2020  12:07 PM               753 20200208232800_0.png
02/11/2020  12:07 PM               314 20200208232951_1.png
02/11/2020  12:07 PM               532 20200208233141_2.png
02/11/2020  12:07 PM               170 20200208233331_3.png
02/11/2020  12:07 PM               381 20200208233526_4.png
02/11/2020  12:07 PM               944 20200208233713_5.png
02/11/2020  12:07 PM                86 20200209010340_6.png
02/11/2020  12:07 PM               826 20200209225638_7.png
02/11/2020  12:07 PM               650 20200209225829_8.png
02/11/2020  11:35 AM        12,115,117 FOX6canpacket.log
02/11/2020  12:02 PM         3,326,511 serverlogs.tar.gz
              11 File(s)     15,446,284 bytes
```

