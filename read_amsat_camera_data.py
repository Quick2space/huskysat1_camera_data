#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import argparse
import tkinter as tk
import sys
import csv
from PIL import Image as img

import os
import tarfile
import requests

from datetime import datetime, date, time
import time 

class picture_recv(object):

    def __init__(self):

        self.xd = 96
        self.yd = 64

        self.ui = False

        # 5 pixels width 
        self.xScaleFactor = 6
        self.yScaleFactor = 5

        self.lastData = 0

        if self.ui:
            self.top = tk.Tk()
            self.C = tk.Canvas(self.top, bg="black", 
                height = self.yd * self.yScaleFactor, 
                width = self.xd * self.xScaleFactor)

        self.next_x_updated = 0
        self.next_y_updated = 0

        self.restartImage = False

        self.pixelsDrawn = 0

        # TODO - remove this after adding rendering in a separate thread
        self.pixelsToBeDisplayedTest = 250

        self.current_x = self.xd + 1
        self.current_y = self.yd + 1

        self.imageIndex = 0
        self.imageInitialized = False
        self.imageModified = False
        self.pixelsReceived = 0
        self.corruptedPixels = 0
        self.totalPixelsReceived = 0

        self.lastDataTimeInSeconds = 0


    def CalculateChecksum(self, data):
        s = 0
        for d in data:
            s = 7 * s + int(d % 255) * 11
            s = s % 255
        # print(data, " ---> ", s)
        return s

    def InitCurrentImage(self, dataTimestamp):
        if not self.imageInitialized:
            self.imageName = "%s_%d.png" % (dataTimestamp, self.imageIndex)
            self.imageIndex += 1
            self.currentImage = img.new('L', (self.xd, self.yd))
            self.pixelsReceived = 0
            self.corruptedPixels = 0
        self.imageInitialized = True

    def SaveCurrentImage(self):
        if self.imageInitialized and self.imageModified:
            if (self.corruptedPixels > 0):
                print("- WARNING: Corrupted pixels detected! %d corrupted pixels" % (self.corruptedPixels))
            print("- Saving image %s (%d pixels) ..." % (self.imageName, self.pixelsReceived))
            self.currentImage.save(self.imageName)
            self.imageModified = False
            self.imageInitialized = False

    def AddPixelToCurrentImage(self, x, y, grayShade):
        try:
            self.currentImage.putpixel( (x,y), (grayShade))
            self.imageModified = True
            self.pixelsReceived += 1
            self.totalPixelsReceived += 1
        except:
            # print("warning: putpixel(%d,%d) failed" % (x,y))
            pass

    def DrawPixels(self, arr, dataTimestamp, dataTimeInSeconds):

        start_x = arr[0]
        start_y = arr[1]
        chksum = arr[7]

        pixels = arr[2:7]
        assert len(pixels) == 5

        received_chksum = self.CalculateChecksum(arr[0:7])

        # Invalid data?
        if chksum != received_chksum:
            self.corruptedPixels += 1
            return False

        previous_x = self.current_x
        previous_y = self.current_y

        self.current_x = start_x
        self.current_y = start_y

        # check to see if we need to save the previous image
        # TODO - improve this code if we have very sparse data!
        
        max_pixel_distance_x_same_picture = 30
        if (((dataTimeInSeconds != self.lastDataTimeInSeconds) and (self.current_x < previous_x)) or (abs(self.current_x - previous_x) > max_pixel_distance_x_same_picture)):
            print("- Pixels rollover detected. Saving the current image ... (current_x = %d, current_y = %d, prev_x = %d, prev_y = %d, current_d = %d, prev_d = %d)" % 
                (self.current_x, self.current_y, previous_x, previous_y, dataTimeInSeconds, self.lastDataTimeInSeconds))
            self.SaveCurrentImage()
            self.InitCurrentImage(dataTimestamp)

        x = start_x
        y = start_y

        unpacked_pixels = []
        for pixel in pixels:
            pixel_binary = "{0:08b}".format(pixel)
            pixel1 = int(pixel_binary[:4], 2)
            pixel2 = int(pixel_binary[4:], 2)
            unpacked_pixels.append(pixel1 * 16)
            unpacked_pixels.append(pixel2 * 16)

        assert len(unpacked_pixels) == 10
        # print(unpacked_pixels)
        while len(unpacked_pixels) > 0:
            grayShade = (int(unpacked_pixels[0])) % 255 
            
            color = "#%02x%02x%02x" % (grayShade, grayShade, grayShade)
            # print("x y", x, y)
            if self.ui:
                self.C.create_rectangle(y * self.yScaleFactor, 
                        x * self.xScaleFactor,
                        (y + 1) * self.yScaleFactor, 
                        (x + 1) * self.xScaleFactor, 
                        fill=color)

            self.pixelsDrawn += 1 

            unpacked_pixels = unpacked_pixels[1:]
            y += 1
            if (y >= self.yd):
                y = 0
                x += 1
                if (x > self.xd):
                    x = 0

            self.AddPixelToCurrentImage(x, y, grayShade)


        self.next_x_updated = x
        self.next_y_updated = y

        # Display some partial progress
        if self.ui:
            self.C.pack()
            self.top.update()

        # TODO - add threading and continuous update
        # if self.pixelsToBeDisplayedTest <= self.pixelsDrawn:
        #    self.top.mainloop()

        return True


    def Read(self, dataTimestamp, dataTimeInSeconds, candata):
        # print('-----------')
        
        if (not self.DrawPixels(candata, dataTimestamp, dataTimeInSeconds)):
            print("!!!error:{0}" % data)
            return False
        else:
            # print("    RECV: {0}".format(recvMsg))
            pass

        self.lastDataTimeInSeconds = dataTimeInSeconds
        self.lastDataTimestamp = dataTimestamp
        self.lastData = candata


def get_CAN_file(to_directory='.'):
    cwd = os.getcwd()
    try:
        os.chdir(to_directory)

        url = 'http://amsat.org/tlm/husky/serverlogs.tar.gz'
        downloadedFile = requests.get(url)         
        open("serverlogs.tar.gz", 'wb').write(downloadedFile.content)
        file = tarfile.open("serverlogs.tar.gz", 'r:gz')
        try: 
            file.extract("FOX6canpacket.log")
        finally: 
            file.close()

        return "FOX6canpacket.log"
    finally:
        os.chdir(cwd)



if __name__ == "__main__":
    print("- Parsing command line ... ")
    parser = argparse.ArgumentParser(description='Parse AMSAT camera data')
    parser.add_argument("--inputFile", help="path to the FOX6canpacket.log file")
    parser.add_argument("--noui", help="No UI display", action='store_false')
    args = parser.parse_args()
    myfile = ""
    if args.inputFile:
        myfile = args.inputFile
    else:
        print("- Downloading CAN file ... ")
        myfile = get_CAN_file()
    print("- CSV input file = %s" % args.inputFile)

    with open(myfile, 'r') as csvfile:
        pr = picture_recv()
        pr.ui = not args.noui
        idx = 0
        csvreader = csv.reader(csvfile)
        for row in csvreader:

            dataTimestamp = row[0]
            ts = datetime.strptime(dataTimestamp, "%Y%m%d%H%M%S")
            dataTimeInSeconds = time.mktime(ts.timetuple())
            # print("- sec = %d, %s, %s" % (lastDataTimeInSeconds, lastDataTimestamp, ts))

            # Extract CAN ID (bytes 6,7,8,9)
            canId = row[6:10]

            # filter out non-camera CAN packets 
            if (canId != ['32', '3', '24', '242']):
                continue

            # Extract CAN data (bytes 10..17)
            canCameraDataString = row[10:]
            canCameraData = list(map(int, canCameraDataString))

            pr.Read(dataTimestamp, dataTimeInSeconds, canCameraData)

            # print(row)
            # print(canId)
            # print(canCameraData)
            idx += 1
            if (idx % 10) == 0: 
                # print("(%d)" % idx)
                pass

        # Save last image if not finished
        pr.SaveCurrentImage()
        print(" - Total pixels = %d" % pr.totalPixelsReceived)
